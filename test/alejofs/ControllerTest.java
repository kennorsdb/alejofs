/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kenno
 */
public class ControllerTest {

    public ControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of crt method, of class Controller.
     */
    @Test
    public void allTest() throws Exception {
        Controller controller = new Controller();

        //Se crea el sistema de archivos
        controller.crt(100, 10, "test.txt");

        //Se crea una serie de archivos en la raiz
        controller.fle("1", "Archivo1");
        controller.fle("2", "Archivo2");
        controller.fle("3", "Archivo3");
        controller.fle("4", "Archivo4");
        controller.fle("5", "Archivo5");
        controller.fle("6", "Archivo6");
        controller.fle("7", "Archivo7");

        // Se crean tres directorios
        controller.mkdir("Dir1");
        controller.mkdir("Dir2");
        controller.mkdir("Dir3");

        // Se crean archivos en cada directorio
        controller.chdir("Dir1");
        controller.fle("1", "Archivo1");
        controller.fle("8", "Archivo8");
        controller.fle("9", "Archivo9");
        controller.fle("10", "Archivo10");
        controller.fle("11", "Archivo11");
        controller.fle("12", "Archivo12");
        controller.fle("13", "Archivo13");
        controller.chdir("/");

        controller.chdir("Dir2");
        controller.fle("1", "Archivo21");
        controller.fle("14", "Archivo22");
        controller.fle("15", "Archivo23");
        controller.fle("16", "Archivo24");
        controller.fle("17", "Archivo25");
        controller.fle("18", "Archivo26");
        controller.fle("19", "Archivo27");
        controller.chdir("/");

        controller.chdir("Dir3");
        controller.fle("1", "Archivo31");
        controller.fle("20", "Archivo32");
        controller.fle("21", "Archivo33");
        controller.fle("22", "Archivo34");
        controller.fle("23", "Archivo3555555555555555");
        controller.fle("24", "Archivo36");
        controller.fle("25", "Archivo37");
        controller.chdir("/");

        //Se muestran los archivos del directorio raíz
        System.out.println("Salida=:");
        controller.ldir();

        controller.chdir("Dir1");
        System.out.println("Salida=:");
        controller.ldir();

        controller.mfle("1", "QWERTYUIO1 de Dir1");
        System.out.println("Salida=:");
        controller.ppt("1");

        System.out.println("Salida=:");
        controller.view("1");

        controller.chdir("..");
        //controller.mfle("1", "RRRRRRRRRRRRRRRRRRRRRRRRRRRS");
        //controller.mfle("3", "EEEEEEEEEEEEEEEEEEEEEEEEEEEQ");
        controller.rem("3");
        controller.mov("4", "Dir1");
        controller.mov("2", "/Dir1");
        controller.chdir("Dir1");
//        controller.mov("/3", ".");
//        controller.mov("../5", ".");
        System.out.println("Salida=:");
        controller.ldir();

        controller.chdir("/");
        //controller.rem("Dir1/1");
        controller.rem("Dir1/8");
        controller.chdir("Dir1");
        System.out.println("Salida=:");
        controller.ldir();
        controller.chdir("/");
        controller.rem("Dir2");
        System.out.println("Salida=:");
        controller.ldir();
        
        System.out.println("Salida=:");    
        controller.find("1");
    }

}
