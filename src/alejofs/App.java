/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.util.Scanner;

/**
 *
 * @author kenno
 */
public class App {

    boolean salir = false;
    Controller controller;

    public App() {
        this.controller = new Controller();
    }

    public void menu() {

        while (!salir) {
            System.out.print(this.controller.prompt());
            Scanner in = new Scanner(System.in);
            String[] comando = in.nextLine().toLowerCase().split(" ");
            controller.parseDTO(new DTO(comando));
            
        }

    }

    public static void main(String[] args) {
        App alejofs = new App();
        alejofs.menu();
    }

}
