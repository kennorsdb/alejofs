/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kenno
 */
public class AlejoLowLevel {

    private final String fileName;
    private final File fs;
    private final RandomAccessFile fsWriter;
    private final long sectorCount;
    private final long sectorSize;
    private TreeMap<Long, Long> freeSectors;

    public AlejoLowLevel(String fileName, long sectorCount, long sectorSize) throws AlejoFSException {
        this.fileName = fileName;
        this.sectorCount = sectorCount;
        this.sectorSize = sectorSize;

        this.freeSectors = new TreeMap<>();

        try {
            fs = new File(fileName);
            fsWriter = new RandomAccessFile(fs, "rw");

            for (int i = 0; i <= sectorCount * sectorSize; i++) {
                writeByte(' ', i);
            }

            freeSectors.put(0L, sectorCount);

        } catch (IOException ex) {
            throw new AlejoFSException("No se puede crear o abrir el archivo del Sistema de Archivos");
        }
    }

    public void remove(AlejoFile f) throws AlejoFSException {
        for (Long sector : f.getSectors().values()) {
            delSector(sector, '#');

            boolean lonely = false;

            if (freeSectors.containsKey(sector + 1)) {
                long end = freeSectors.get(sector + 1);
                freeSectors.remove(sector + 1);
                freeSectors.put(sector, end);
                lonely = false;
            } else {
                lonely = true;
            }
            if (freeSectors.containsValue(sector - 1)) {
                long start = freeSectors.lowerKey(sector);

                if (freeSectors.get(sector) != null) {
                    long end = freeSectors.get(sector);
                    freeSectors.remove(sector);
                    freeSectors.remove(start);
                    freeSectors.put(start, end);
                } else {
                    freeSectors.remove(start);
                    freeSectors.put(start, sector);
                }

                lonely = false;
            } else {
                lonely = true;
            }

            if (lonely) {
                freeSectors.put(sector, sector);
            }
        }
        
         f.getSectors().clear();

    }

    public void delSector(long sector, char c) throws AlejoFSException {
        for (long i = sector * sectorSize; i < (sector + 1) * sectorSize; i++) {
            writeByte(c, i);
        }

    }

    public void write(AlejoFile f, String content) throws AlejoFSException {
        long size = content.length();
        long contentSectors = (long) Math.ceil(size / (float) sectorSize);
        long originalSectors = f.getSectorCount();
        long sizeSectors = 0;

        // Se revisa si se necesitan más sectores
        if (f.getSectorCount() < contentSectors) {
            sizeSectors = contentSectors - f.getSectorCount();
        } else if (f.getSectorCount() <= 0) {
            sizeSectors = contentSectors;
        }

        // Se asignan los nuevos sectores
        boolean hayEspacio = false;
        if (sizeSectors != 0) {
            long startSector = -1;
            for (Map.Entry<Long, Long> e : freeSectors.entrySet()) {
                if (e.getValue() - e.getKey() >= sizeSectors) {
                    startSector = e.getKey();
                    freeSectors.remove(startSector);
                    freeSectors.put(startSector + sizeSectors, e.getValue());
                    for (int i = 0; i < sizeSectors; i++) {
                        f.addSector(startSector + i);
                    }
                    hayEspacio = true;
                    break;
                }
            }
        }

        if (!hayEspacio) {
            // Revisamos si existe suficiente espacio en el disco
            long free = 0;
            for (Map.Entry<Long, Long> e : freeSectors.entrySet()) {
                free += e.getValue() - e.getKey();
            }

            if (free >= sizeSectors) {
                for (Map.Entry<Long, Long> e : freeSectors.entrySet()) {
                    if (e.getValue() - e.getKey() <= sizeSectors) {
                        sizeSectors -= e.getValue() - e.getKey();
                        freeSectors.remove(e.getKey());
                        for (int i = 0; i < sizeSectors; i++) {
                            f.addSector(e.getKey() + i);
                        }
                    } else {
                        long startSector = e.getKey();
                        freeSectors.remove(startSector);
                        freeSectors.put(startSector + sizeSectors, e.getValue());
                        for (int i = 0; i < sizeSectors; i++) {
                            f.addSector(startSector + i);
                        }
                        break;
                    }

                }
            } else {
                throw new AlejoFSException("No hay espacio suficiente en el FS");
            }
        }

        for (int i = 0; i < f.getSectorCount() * sectorSize; i++) {
            long b = f.getSectors().get(Math.floorDiv(i, sectorSize)) * sectorSize + i % sectorSize;
            if (i < content.length()) {
                this.writeByte(content.charAt(i), b);
            } else {
                this.writeByte(' ', b);
            }
        }

        if (contentSectors < originalSectors) {
            for (long sector = contentSectors; sector < originalSectors; sector++) {
                f.getSectors().remove(sector);
                delSector(sector, '#');

                boolean lonely = false;

                if (freeSectors.containsKey(sector + 1)) {
                    long end = freeSectors.get(sector + 1);
                    freeSectors.remove(sector + 1);
                    freeSectors.put(sector, end);
                    lonely = false;
                } else {
                    lonely = true;
                }
                if (freeSectors.containsValue(sector - 1)) {
                    long start = freeSectors.lowerKey(sector);

                    if (freeSectors.get(sector) != null) {
                        long end = freeSectors.get(sector);
                        freeSectors.remove(sector);
                        freeSectors.remove(start);
                        freeSectors.put(start, end);
                    } else {
                        freeSectors.remove(start);
                        freeSectors.put(start, sector);
                    }

                    lonely = false;
                } else {
                    lonely = true;
                }

                if (lonely) {
                    freeSectors.put(sector, sector);
                }

            }
        }
        System.out.println(freeSectors.toString());
    }

    public String readSector(long sector) throws AlejoFSException {
        try {
            String s = "";
            fsWriter.seek(sector * sectorSize );

            for (int i = 0; i < sectorSize; i++) {
                s += (char) fsWriter.readByte();
            }
            return s;
        } catch (IOException ex) {
            throw new AlejoFSException("No se pudo leer el FS");
        }
    }

    public void writeByte(char c, long seek) throws AlejoFSException {
        try {
            fsWriter.seek(seek);
            fsWriter.writeByte(c);
        } catch (IOException ex) {
            throw new AlejoFSException("Problemas al escribir en archivo");
        }

    }

}
