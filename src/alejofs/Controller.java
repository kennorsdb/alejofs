/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author kenno
 */
public class Controller {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    AlejoFS alejoFS;

    public Controller() {
        alejoFS = null;
    }

    /*
    CRT: Este comando lo utilizaremos para crear un disco virtual. 
       Los parámetros serán la cantidad de sectores, el tamaño del sector y 
       un nombre que le identificará la raíz.
     */
    public void crt(int sectorCount, int sectorSize, String name) throws AlejoFSException {
        alejoFS = new AlejoFS(name, sectorCount, sectorSize);

    }

    /*
    FLE: Crear un Archivo. Se le debe definir el contenido del archivo 
    y el nombre y extensión de este.
     */
    public void fle(String name, String content) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.createFile(name, content);
    }

    /*
     MKDIR: Este comando crea un directorio en el directorio Actual. 
    El parámetro es el nombre del directorio.
     */
    public void mkdir(String name) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.makeDir(name);

    }

    /*
     CHDIR: Permite cambiar el directorio actual. Me debe permitir irme a un 
    directorio cualquiera de la estructura de directorios actual
     */
    public void chdir(String newDirectory) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.changeDir(newDirectory);

    }

    /*
     LDIR: Lista los archivos y directorios dentro del directorio actual. 
    Debe mostrar una diferencia clara entre los directorios y archivos.
     */
    public void ldir() throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        for (AlejoEntity e : alejoFS.getActualDirectoryFiles()) {
            if (e instanceof AlejoDirectory) {
                System.out.println(ANSI_GREEN + ((AlejoDirectory) e).getName() + "/");
            } else {
                System.out.println(ANSI_YELLOW + e.getName());
            }
        }
    }

    /*
     MFLE: Se puede seleccionar un archivo y cambiarle el contenido.
     */
    public void mfle(String file, String content) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.changeFileContent(file, content);
    }

    /*
     PPT: Permite ver las propiedades de un archivo. Nombre, Extensión, 
    Fecha de Creación, Fecha de Modificación y tamaño.
     */
    public void ppt(String file) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        AlejoEntity e = alejoFS.getEntity(file);

        if (e != null) {
            String[] name = e.getName().split("\\.");
            System.out.println(ANSI_GREEN + "Nombre: " + ANSI_YELLOW + name[0] + "\n");
            if (name.length > 1) {
                System.out.println(ANSI_GREEN + "Extensión: " + ANSI_YELLOW + name[name.length - 1]);
            }
            System.out.println(ANSI_GREEN + "Fecha de Creación: " + ANSI_YELLOW + e.getCreationDate().format(DateTimeFormatter.ISO_DATE) + "\n");
            System.out.println(ANSI_GREEN + "Fecha de Modificación: " + ANSI_YELLOW + e.getModificationDate().format(DateTimeFormatter.ISO_DATE) + "\n");
            System.out.println(ANSI_GREEN + "Tamaño: " + ANSI_YELLOW + e.getSize() + "\n");
        }

    }

    /*
     VIEW: Para un determinado archivo se debe poder ver el contenido del archivo.
     */
    public void view(String file) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        AlejoEntity e = alejoFS.getEntity(file);

        if (e != null && e instanceof AlejoFile) {
            System.out.println(ANSI_GREEN + "Nombre del Archivo: " + ANSI_YELLOW + e.getName() + ANSI_GREEN + ", contenido:\n");
            System.out.println(ANSI_RESET + ((AlejoFile) e).getContent() + "\n");
        }
    }

    /*
    CPY: Se implementarán 3 tipos de copies. (aplica a archivos o directorios) / 
            - un archivo con ruta “real” será copiado a una ruta “virtual” de MI File System. 
            - un archivo con ruta “vitual” de MI File System será copiado a una ruta “real” 
            - un archivo con ruta “vitual” de MI File System será copiado a otra ruta “virtual” de MI File System.
     */
    public void cpy(String source, String dest) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        if (source == null || dest == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        } else {
            alejoFS.copyFile(source, dest);
        }
    }

    /*
     MOV: Mover un archivo o directorio. Nótese que el MV sirve como rename, 
          pues se puede mover al mismo directorio con otro nombre.
     */
    public void mov(String file, String dest) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.moveFile(file, dest);
    }

    /*
     REM: Con este comando se eliminaran archivos. 
          La eliminación puede ser normal para uno o varios archivos 
          o recursiva en el caso de directorios.
     */
    public void rem(String file) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        alejoFS.deleteFile(file);
    }

    /*
     TREE: Despliega, simulando un árbol, la estructura de archivos del File System. 
           Debe estar visible siempre.
     */
    public void tree() throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }
        
        String out = alejoFS.treeFS();
        System.out.println(out);
        
    }

    /*
     FIND: Recibe un nombre de archivo o directorio y lista todas las rutas de 
           MI File System con archivos de ese nombre. 
           (Nótese que puede recibir de parámetro algo como “*.doc”) 
     */
    public void find(String file) throws AlejoFSException {
        if (alejoFS == null) {
            throw new AlejoFSException("Se debe crear un sistema de archivo antes de cualquier operación");
        }

        for (AlejoEntity e : alejoFS.findFile(file)) {
            if (e instanceof AlejoDirectory) {
                System.out.println(ANSI_GREEN + ((AlejoDirectory) e).getPath() + "/");
            } else {
                System.out.println(ANSI_YELLOW + e.getPath() + "");
            }
        }
    }

    public String prompt() {
        if (alejoFS != null) {
            return ANSI_GREEN + "AlejoFS => " + ANSI_YELLOW  + alejoFS.actualDirPath() + "/ " + ANSI_GREEN + "--> ";
        } else 
            return ANSI_YELLOW  + " ~~~ " + ANSI_GREEN + "--> ";
    }
    

    public void parseDTO(DTO dto) {
        try {
            switch (dto.getComando(0)) {
                case "crt":
                    crt(dto.getComandoInt(1), dto.getComandoInt(2), dto.getComando(3));
                    break;
                case "fle":
                    fle(dto.getComando(1), dto.getComando(2));
                    break;
                case "mkdir":
                    mkdir(dto.getComando(1));
                    break;
                case "chdir":
                    chdir(dto.getComando(1));
                    break;
                case "ldir":
                    ldir();
                    break;
                case "mfle":
                    mfle(dto.getComando(1), dto.getComando(2));
                    break;
                case "ppt":
                    ppt(dto.getComando(1));
                    break;
                case "view":
                    view(dto.getComando(1));
                    break;
                case "cpy":
                    cpy(dto.getComando(1), dto.getComando(2));
                    break;
                case "mov":
                    mov(dto.getComando(1), dto.getComando(2));
                    break;
                case "rem":
                    rem(dto.getComando(1));
                    break;
                case "tree":
                    tree();
                    break;
                case "find":
                    find(dto.getComando(1));
                    break;
                default:
                    dto.setErrorFlag(true);
                    dto.setErrorMsg("Comando no reconocido");
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            dto.setErrorFlag(true);
            dto.setErrorMsg("Faltan parámetros en el comando.");
        } catch (NumberFormatException e) {
            dto.setErrorFlag(true);
            dto.setErrorMsg("Es necesario agregar un número entero en uno de los parámetros. " + e.getMessage());
        } catch (AlejoFSException e) {
            dto.setErrorFlag(true);
            dto.setErrorMsg(e.getMessage());
        }

        if (dto.isErrorFlag()) {
            System.out.println(ANSI_RED + "Error en el Comando: " + dto.getComando(0));
            System.out.println(ANSI_RED + dto.getErrorMsg());
        }
    }
}
