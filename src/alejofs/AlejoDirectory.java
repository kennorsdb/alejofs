/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.util.ArrayList;

/**
 *
 * @author kenno
 */
public class AlejoDirectory extends AlejoEntity {

    ArrayList<AlejoEntity> files;

    public AlejoDirectory(AlejoDirectory parent, String name) throws AlejoFSException {
        super(parent, name, null);

        files = new ArrayList<>();
    }

    public void insertFile(AlejoEntity file) throws AlejoFSException {
        if (file == null) {
            throw new AlejoFSException("File null in insertion");
        }

        files.add(file);
    }

    public void removeChild(AlejoEntity file) throws AlejoFSException {
        if (file == null) {
            throw new AlejoFSException("File null in insertion");
        }

        files.remove(file);
    }

    @Override
    void delete() throws AlejoFSException {
        for (AlejoEntity f : files){
            if(f instanceof AlejoDirectory)
                f.delete();
        } 
        
        files.clear();
    }
    
    

    public ArrayList<AlejoEntity> findRecursively(String name) {
        ArrayList<AlejoEntity> res = new ArrayList<>();
        for (AlejoEntity a : files) {
            if (a.getName().matches(name)) {
                res.add(a);
            }

            if (a instanceof AlejoDirectory) {
                res.addAll(((AlejoDirectory) a).findRecursively(name));
            }
        }
        return res;
    }

    public AlejoEntity getEntity(String name) {
        name = name.toLowerCase();
        if (name.equals(".")) {
            return this;
        } else if (name.equals("..")) {
            return parent;
        } else {
            for (AlejoEntity a : files) {
                if (a.name.toLowerCase().equals(name)) {
                    return a;
                }
            }
        }

        return null;
    }

    public ArrayList<AlejoEntity> getFiles() {
        return files;
    }
    
    @Override
    public String tree(int level){
        String start = "";
        for (int i = 0; i < level; i++) {
            start += "|   ";
        }
        
        String res = name + "\n";
        for (int i = 0; i < files.size()-1; i++) {
            res += start + "|-- " + files.get(i).tree(level+1) ;
        }
        
        if (files.size() >= 1) {
            res += start + "`-- " + files.get(files.size()-1).tree(level+1);
        }
        return res;
        
    }

}
