/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.time.LocalDateTime;
import java.util.TreeMap;

/**
 *
 * @author kenno
 */
public class AlejoFile extends AlejoEntity {

    private long size; // En bytes
    private AlejoLowLevel alejofs;
    private final TreeMap<Long, Long> sectors;

    @Override
    public long getSize() {
        return size;
    }

    public long getSectorCount() {
        return sectors.size();
    }

    public void addSector(long sector) {
        if (sectors.size() <= 0) {
            sectors.put(0L, sector);
        } else {
            sectors.put(sectors.lastKey() + 1, sector);
        }

    }

    public TreeMap<Long, Long> getSectors() {
        return sectors;
    }

    public AlejoFile(AlejoDirectory parent, AlejoLowLevel alejofs, String name, String extension) throws AlejoFSException {
        super(parent, name, extension);

        size = 0;

        this.alejofs = alejofs;
        this.sectors = new TreeMap<>();

    }

    @Override
    void delete() throws AlejoFSException {
        alejofs.remove(this);
    }

    public String getContent() throws AlejoFSException{
        String r = "";
        for(Long sector : sectors.values()){
            r += alejofs.readSector(sector);
        }
        return r;
    }
    
    
    void setContent(String content) throws AlejoFSException {
        if (content == null) {
            throw new AlejoFSException("El contenido del archivo no puede ser nulo");
        }

        alejofs.write(this, content);

        this.size = content.length();
        this.modificationDate = LocalDateTime.now();
    }

    AlejoEntity find(String fileName) {
        if (fileName.equals(this.name)) {
            return this;
        } else {
            return null;
        }
    }
;

}
