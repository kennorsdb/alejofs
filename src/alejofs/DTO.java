/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

/**
 *
 * @author kenno
 */
public class DTO {
    private String [] comando;
    private String errorMsg;
    private boolean errorFlag;
    private String resultado;

    public DTO(String[] comando) {
        this.comando = comando;
        errorFlag = false;
    }

    
    
    public String[] getComando() {
        return comando;
    }
    
    public String getComando(int i) throws ArrayIndexOutOfBoundsException{
        return comando[i];
    }
    
    public int getComandoInt(int i) throws 
            ArrayIndexOutOfBoundsException, 
            NumberFormatException {
        return Integer.parseInt(comando[i]);
    }

    public void setComando(String[] comando) {
        this.comando = comando;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(boolean errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    
}
