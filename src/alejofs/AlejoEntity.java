/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author kenno
 */
public class AlejoEntity {
    protected AlejoDirectory parent;
    protected String name;
    protected String extension;  
    
    protected LocalDateTime creationDate;
    protected LocalDateTime modificationDate;

    public AlejoDirectory getParent() {
        return parent;
    }

    public void setParent(AlejoDirectory parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getModificationDate() {
        return modificationDate;
    }
    
    public long getSize() {
        return 0;
    }
    

    
    public AlejoEntity(AlejoDirectory parent, String name, String extension) throws AlejoFSException {
        
        if ( name == null ){
            throw new AlejoFSException("El nombre del archivo no puede ser nulo");
        }
        
        this.parent = parent;
        this.name = name;
        this.extension = extension;
        
        creationDate = LocalDateTime.now();
        modificationDate = LocalDateTime.now();

    }
    
    
    void delete() throws AlejoFSException { };
    
    
    
    public void rename(String fileName) throws AlejoFSException{
        if (fileName == null){
            throw new AlejoFSException("Nombre de archivo incorrecto");
        }      
        this.name = fileName;
    };
    
    
    
    public void move ( AlejoDirectory dir ) throws AlejoFSException{
               if (dir == null){
            throw new AlejoFSException("Nombre de archivo incorrecto");
        }
        
        this.parent.removeChild(this);
        this.parent = dir; 
        this.parent.insertFile(this);
    }
    
    boolean sameFileName(String fileName) {
        return fileName == this.name;
    }
    
    AlejoEntity find(String fileName) {
        if (fileName.equals(this.name))
            return this;
        else
            return null;
    };
    
    public String getPath() {
        if (parent == null)
            return "";
        else 
            return parent.getPath() + "/" + getName();
    }
    
    public String tree(int level){
        return name + "\n";
    }

    
    
}
