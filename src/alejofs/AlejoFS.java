/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alejofs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author kenno
 */
public class AlejoFS {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    private final String fileName;
    private final long sectorCount;
    private final long SectorSize;

    private AlejoLowLevel low;
    private AlejoDirectory root;
    private AlejoDirectory actualDir;

    public AlejoFS(String fileName, long sectorCount, long SectorSize) throws AlejoFSException {

        this.fileName = fileName;
        this.sectorCount = sectorCount;
        this.SectorSize = SectorSize;

        actualDir = root = new AlejoDirectory(null, "/");
        
        low = new AlejoLowLevel(fileName, sectorCount, SectorSize);
    }

    public void createFile(String name, String content) throws AlejoFSException {
        if (actualDir.getEntity(name) != null) {
            throw new AlejoFSException("El nombre del archivo ya existe en el directorio");
        }

        AlejoFile file = new AlejoFile(actualDir, low, name, ".txt");
        file.setContent(content);
        actualDir.insertFile(file);
    }

    public void makeDir(String name) throws AlejoFSException {
        if (actualDir.getEntity(name) != null) {
            throw new AlejoFSException("El nombre del directorio ya existe.");
        }

        AlejoDirectory dir = new AlejoDirectory(actualDir, name);
        actualDir.insertFile(dir);
    }

    public void changeDir(String dirName) throws AlejoFSException {
        if (dirName.equals("..")) {
            actualDir = actualDir.getParent();
        } else if (dirName.equals("/")) {
            actualDir = root;
        } else {
            AlejoEntity dir = actualDir.getEntity(dirName);
            if (dir != null && dir instanceof AlejoDirectory) {
                actualDir = (AlejoDirectory) dir;
            } else {
                throw new AlejoFSException("No se encuentra un subdirectorio con ese nombre.");
            }
        }
    }

    public ArrayList<AlejoEntity> getActualDirectoryFiles() {
        return actualDir.getFiles();
    }

    public void changeFileContent(String name, String content) throws AlejoFSException {
        AlejoEntity file = actualDir.getEntity(name);

        if (file != null) {
            if (file instanceof AlejoFile) {
                ((AlejoFile) file).setContent(content);
            } else {
                throw new AlejoFSException("El archivo es un directorio.");
            }
        } else {
            throw new AlejoFSException("No se encuentra el archivo");
        }

    }

    public AlejoEntity getEntity(String fileName) {
        return actualDir.getEntity(fileName.toLowerCase());
    }

    public AlejoEntity parsePath(String path) throws AlejoFSException {
        if (path == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }
        
        if(path.equals("/"))
            return root;
        
        String[] tokens = path.toLowerCase().split("/");
        AlejoEntity e = null;
        int i = 0;
        if (tokens.length > 0 && tokens[0].equals("")) {
            e = root;
            i++;
        } else {
            e = actualDir;
        }

        for (; i < tokens.length ; i++ ) {
            if (e instanceof AlejoDirectory) {
                e = ((AlejoDirectory) e).getEntity(tokens[i]);
                if (e == null) {
                    throw new AlejoFSException("No se encuentra la ruta especificada.");
                }
            } else if (e instanceof AlejoFile) {
                return e;
            }
        }

        return e;
    }

    public void moveFile(String sourcePath, String destPath) throws AlejoFSException {
        if (sourcePath == null || destPath == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }
        AlejoEntity source = parsePath(sourcePath);
        AlejoEntity dest = parsePath(destPath);

        
        if (dest instanceof AlejoDirectory) {
            if(((AlejoDirectory) dest).getEntity(source.getName()) == null){
                source.move((AlejoDirectory) dest);
            } else {
                throw new AlejoFSException("Ya existe un archivo con el mismo nombre de la fuente.");
            }
        } else {
            throw new AlejoFSException("El destino debe ser un directorio");
        }

    }

    public void deleteFile(String path) throws AlejoFSException {
        if (path == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }

        AlejoEntity f = parsePath(path);
        f.delete();
        f.getParent().removeChild(f);
        
    }

    public ArrayList<AlejoEntity> findFile(String file) throws AlejoFSException {
        if (file == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }

        return root.findRecursively(file.replace(".", "\\."));
    }
    
    public void copyFile(String source, String dest) throws AlejoFSException{
        String [] sourceParse = source.split(":-");
        String [] destParse = dest.split(":-");
        
        //Copy de virtual a virtual
        if(sourceParse.length>1 && destParse.length>1){
            AlejoEntity s, d;
            s = parsePath(sourceParse[1]);
            d = parsePath(destParse[1]);
            if(!(d instanceof AlejoDirectory)){
                throw new AlejoFSException("Destino debe ser un directorio");
            }
            else{
                if(((AlejoDirectory) d).getEntity(s.getName()) == null){
                    ((AlejoDirectory) d).insertFile(s);
                }
                else{
                    throw new AlejoFSException("Ya existe un archivo con el mismo nombre de la fuente.");
                }
            }      
        }
        
        // Copy de virtual a real
        else if(sourceParse.length>1){
            AlejoEntity s = parsePath(sourceParse[1]);
            if(!(s instanceof AlejoFile)){
                throw new AlejoFSException("La direccion fuente debe ser un archivo");
            }
            else{
                String realDest = dest+s.getName();
                virtualToReal(((AlejoFile)s).getContent(), realDest);
            }
        }
        
        // Copy de real a virtual
        else if(destParse.length>1){
            AlejoEntity d = parsePath(destParse[1]);
            if(!(d instanceof AlejoDirectory)){
                throw new AlejoFSException("Destino debe ser un directorio");
            }
            else{
                String content = realToVirtual(source);
                
                // TODO: ver como parsear el filename y la extension
                File fileFormat = new File(source);
                AlejoFile newfile = new AlejoFile((AlejoDirectory)d, low, fileFormat.getName(), "");
                ((AlejoDirectory) d).insertFile(newfile);
            }
        }
        else
            throw new AlejoFSException("No se puede copiar de ruta real a otra ruta real");
    }
    
    private String realToVirtual(String file) throws AlejoFSException{
        if (file == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }        
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            String content = "";
            while((line = bufferedReader.readLine()) != null) {
                content+=line;
            }   
            bufferedReader.close();
            
            return content;

        } catch (FileNotFoundException ex) {
            throw new AlejoFSException("El archivo no existe");
        } catch (IOException ex) {
            throw new AlejoFSException("Error leyendo el archivo");
        }
        
    }
    
    private void virtualToReal(String file, String path) throws AlejoFSException{
        if (file == null || path == null) {
            throw new AlejoFSException("Dirección de archivo incorrecta");
        }
        try(FileReader fr = new FileReader(path)){
            throw new AlejoFSException("El archivo ya existe en la ruta real");
        } catch (IOException ex) {
            
            try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))){
                bufferedWriter.write(file);
                bufferedWriter.close();
            } catch (IOException ex2) {
                throw new AlejoFSException("Error al escribir archivo");
            }
            
        }
    }
    
    public String treeFS(){
        return root.tree(0);
    }

    public String actualDirPath() {
        return actualDir.getPath();
    }
}
